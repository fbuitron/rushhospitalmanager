//
//  ManagerContainerViewController.swift
//  RushHospitalManager
//
//  Created by Juan Garcia on 5/19/15.
//  Copyright (c) 2015 TANGOCODE. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ManagerContainerViewController: UIViewController {
    
    var _headerView: UIView!
    var _footerView: UIView!
    var _bannerImage: UIImageView!
    var _dateLabel: UILabel!
    var _timeLabel: UILabel!


    @IBOutlet weak var bodyContainer: UIView!
    
    override func viewDidLoad() {
            super.viewDidLoad()
            self.view.addSubview(headerView)
            self.view.addSubview(footerView)
            self.view.addSubview(dateLabel)
            self.view.addSubview(timeLabel)
            self.updateViewConstraints()
    }
    
    override func updateViewConstraints()
    {
        headerView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.view.snp_top)
            make.width.equalTo(self.view.snp_width)
            make.height.equalTo(70)
        }
        
        footerView.snp_makeConstraints { (make) -> Void in
            make.bottom.equalTo(self.view.snp_bottom)
            make.width.equalTo(self.view.snp_width)
            make.height.equalTo(55)
        }
        
        dateLabel.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(self.headerView.snp_left).offset(10)
            make.top.equalTo(self.headerView.snp_top).offset(20)
        }
        
        timeLabel.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(self.headerView.snp_right).offset(-10)
            make.top.equalTo(self.headerView.snp_top).offset(20)
        }

        
        _bannerImage.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(self.footerView.snp_width)
            make.bottom.equalTo(self.footerView.snp_bottom)

        }
        
        super.updateViewConstraints()
        
    }
    
    var headerView : UIView
        {
        get{
            if _headerView == nil
            {
                _headerView = UIView(frame: CGRectMake(100, 100, 100, 100))
                _headerView.backgroundColor = CommonUtilities.RGBColor(RHblueColor)
            }
            return _headerView!
        }
    }
    
    var footerView : UIView
        {
        get{
            if _footerView == nil
            {
                _footerView = UIView(frame: CGRectMake(100, 100, 100, 100))
                _footerView.backgroundColor = CommonUtilities.RGBColor(RHblueColor)
                var bannerImage = UIImage(named: "footerBanner")
                _bannerImage = UIImageView(image: bannerImage)
                _bannerImage.contentMode = UIViewContentMode.ScaleToFill
                //_bannerImage.frame = CGRectMake(0, 0, self.footerView.frame.width, self.footerView.frame.height);
                _footerView.addSubview(_bannerImage)
            }
            return _footerView!
        }
    }
    
    var dateLabel: UILabel
    {
        get{
            
            if _dateLabel == nil
            {
                _dateLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 120, height: 60))
                _dateLabel.text = "Wednesday May 20th"
                _dateLabel.font =  UIFont(name: "Avenir-Medium", size: 20)
                _dateLabel.textColor = UIColor.whiteColor()
            }
            
            return _dateLabel
        }
        
    }
    
    var timeLabel: UILabel
        {
        get{
            
            if _timeLabel == nil
            {
                _timeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 120, height: 60))
                _timeLabel.text = "4:48 PM"
                _timeLabel.font =  UIFont(name: "Avenir-Medium", size: 20)
                _timeLabel.textColor = UIColor.whiteColor()
            }
            
            return _timeLabel
        }
        
    }
   
    
    
}