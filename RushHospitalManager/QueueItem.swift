//
//  QueueItem.swift
//  RushHospitalManager
//
//  Created by Franklin Buitron on 5/14/15.
//  Copyright (c) 2015 TANGOCODE. All rights reserved.
//

import Foundation

class QueueItem
{
    var queueID : Int
    var patientId : Int
    var firstName : String
    var lastName : String
    var waitingNumber : Int
    var waitingMinutes : String
    var treatmentName : String
    var dateTimeAdded : NSDate
    var dateTimeMeeting : NSDate
    
    init  (queueId : Int, patientId : Int, firstName : String, lastName : String, waitingNumber : Int, treatmentName : String, dateTimeAdded : NSDate, dateTimeMeeting : NSDate, waitingMinutes: String )
    {
        self.queueID = queueId
        self.patientId = patientId
        self.firstName = firstName
        self.lastName = lastName
        self.waitingNumber = waitingNumber
        self.treatmentName = treatmentName
        self.dateTimeAdded = dateTimeAdded
        self.dateTimeMeeting = dateTimeMeeting
        self.waitingMinutes = waitingMinutes
    }
    
}