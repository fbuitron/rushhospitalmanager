//
//  ViewController.swift
//  RushHospitalManager
//
//  Created by Franklin Buitron on 5/13/15.
//  Copyright (c) 2015 TANGOCODE. All rights reserved.
//

import UIKit

class ViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var queueList: UITableView!
    var items: [QueueItem] = []
    var timer: NSTimer = NSTimer()
    var doctorNames : [String] = ["Dr. Kogan", "Dr. Cole", "Dr. Verma", "Dr. Kogan", "Dr. Cole", "Dr. Verma", "Dr. Kogan", "Dr. Cole", "Dr. Verma"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println("Se ejecuta el view did load")
        //self.queueList.registerClass(WaitingListCell.self, forCellReuseIdentifier: "CustomCell")
        
        //self.queueList.registerNib(UINib(nibName: "WaitingListCell", bundle: nil),
          //  forCellReuseIdentifier: "CustomCell")
        self.queueList.delegate = self
        self.queueList.dataSource = self
        
        timer = NSTimer.scheduledTimerWithTimeInterval(5, target:self, selector: Selector("timerLapse"), userInfo: nil, repeats: true)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func tableView(tableView:UITableView, heightForRowAtIndexPath indexPath:NSIndexPath)->CGFloat
    {
        return 100
    }
    
    func timerLapse()
    {
        println("Se Ejecuta el timer")
        let data : NSData = getJSON("http://dev.genexususa.com/RushDemo/aGetQueue.aspx")
        let dict : NSArray = parseJSON(data)
        
        let elements = dict.count
        
        var i = 0
        items.removeAll()
        var item : QueueItem

        for (i=0;i<elements;i++)
        {
            
            var oneElement : NSDictionary = dict[i] as! NSDictionary
            println(oneElement)
            let firstName : String = oneElement.valueForKey("PatientFirstName") as! String
           
            let timeAdded : NSDate = convertStringToDate(oneElement.valueForKey("InQueueDateTimeAdded") as! String)
            let timeMeeting : NSDate = convertStringToDate(oneElement.valueForKey("InQueueDateTimeMeeting") as! String)
            
            item = QueueItem(queueId: oneElement.valueForKey("InQueueId") as! Int, patientId: oneElement.valueForKey("PatientId") as! Int, firstName: oneElement.valueForKey("PatientFirstName") as! String, lastName: oneElement.valueForKey("PatientLastName") as! String, waitingNumber: oneElement.valueForKey("InQueueNumberWaiting") as! Int, treatmentName: oneElement.valueForKey("TreatmentName") as! String, dateTimeAdded: timeAdded, dateTimeMeeting: timeMeeting, waitingMinutes: oneElement.valueForKey("InQueueWaitingMinutes") as! String)
            items.append(item)
            
            
        }
        
        
        queueList.reloadData()
    }
    
    func convertStringToDate(aDateInString :String)->NSDate
    {
        println("the string that comes \(aDateInString)")
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.dateFromString(aDateInString)
        println("the string that comes \(date)")
        return date!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: WaitingListCell = self.queueList.dequeueReusableCellWithIdentifier("WaitingListCell") as! WaitingListCell
        
        //if (cell == nil)
        //{
     //   cell = WaitingListCell()
        //}
        println("Going to add this patient \(self.items[indexPath.row].lastName)")
        //cell.completeName?.text = self.items[indexPath.row].lastName
        
        //cell.textLabel?.text = self.items[indexPath.row].firstName
        
        cell.completeName?.text = self.items[indexPath.row].firstName+" "+self.items[indexPath.row].lastName
        cell.queueNumber?.text = String(self.items[indexPath.row].waitingNumber)
        cell.treatmentName?.text = self.items[indexPath.row].treatmentName
        
        var waitingMinutes = self.items[indexPath.row].waitingMinutes.stringByReplacingOccurrencesOfString("minutes", withString: "min")

        cell.watingTime?.text = waitingMinutes
        
        let newDateFormatter = NSDateFormatter()
        newDateFormatter.dateFormat = "hh:mm aa"
        let str = newDateFormatter.stringFromDate(self.items[indexPath.row].dateTimeMeeting)
        
        var tmp = self.items[indexPath.row].dateTimeAdded
        
        cell.meetingTime.text = str
        cell.doctorName.text = self.doctorNames[indexPath.row]
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    func getJSON(urlToRequest: String) -> NSData{
        return NSData(contentsOfURL: NSURL(string: urlToRequest)!)!
    }
    
    func parseJSON(inputData: NSData) -> NSArray{
        var error: NSError?
        var boardsDictionary: NSArray = NSJSONSerialization.JSONObjectWithData(inputData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSArray
        
        return boardsDictionary
    }



}

