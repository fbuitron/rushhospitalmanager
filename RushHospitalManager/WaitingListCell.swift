//
//  WaitingListCell.swift
//  RushHospitalManager
//
//  Created by Franklin Buitron on 5/14/15.
//  Copyright (c) 2015 TANGOCODE. All rights reserved.
//

import UIKit

class WaitingListCell: UITableViewCell {
    
    @IBOutlet var completeName : UILabel!
    @IBOutlet var queueNumber : UILabel!
    @IBOutlet var watingTime : UILabel!
    @IBOutlet var treatmentName : UILabel!
    @IBOutlet weak var meetingTime: UILabel!
    @IBOutlet weak var doctorName: UILabel!
    

}
