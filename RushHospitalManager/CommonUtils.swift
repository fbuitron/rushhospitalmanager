//
//  CommonUtils.swift
//  iBeaconDemoConcept
//
//  Created by Juan Garcia on 5/11/15.
//  Copyright (c) 2015 TangoCode. All rights reserved.
//

import Foundation
import UIKit

let BEACON_REGION:String = "62C2256A-32F2-44FC-8058-7FCC5FAC01CF";
let MAJOR_REGION:Int = 301;
let GXFontNameGizmo = "SSGizmo";
let RHblueColor: String = "#35528D";



enum PatientAppStates {
    case Away
    case CheckIn
    case CheckedIn
    case GUideMe
    case OnSurvey
}


class CommonUtilities: NSObject {
    
    class func getWelcomeMessage(checkInTime: String) -> String
    {
        return " Welcome to Midwest Orthopedics at Rush for your appointment today at \(checkInTime)"
    }
    
    class func getApproachingMessage(lastName: String) -> String
    {
        return " Hello Mr. \(lastName),  welcome to Midwest Othopaedics"
    }
    
    class func getCheckInMessage() -> String
    {
        return "You have been checked in. Please go to the receptionist to fill out any paperwork needed."
    }
    
    class func getFeedbackMessage() -> String
    {
        return "We would love your feedback."
    }
    
    class func getFinilizedSurveyMessage() -> String{
        return "We appreciate your feedback."
    }
    
    class func SendLocalNotification(message: String)
    {
        var localNotification = UILocalNotification()
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 0)
        localNotification.alertBody = message
        localNotification.timeZone = NSTimeZone.defaultTimeZone()
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
    
    class func RGBColor(hexColor : NSString) -> UIColor{
        
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        var hexColorCode = hexColor as String
        
        if hexColorCode.hasPrefix("#")
        {
            let index   = advance(hexColorCode.startIndex, 1)
            let hex     = hexColorCode.substringFromIndex(index)
            let scanner = NSScanner(string: hex)
            var hexValue: CUnsignedLongLong = 0
            
            if scanner.scanHexLongLong(&hexValue)
            {
                if count(hex) == 6
                {
                    red   = CGFloat((hexValue & 0xFF0000) >> 16) / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)  / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF) / 255.0
                }
                else if count(hex) == 8
                {
                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                }
                else
                {
                    print("invalid hex code string, length should be 7 or 9")
                }
            }
            else
            {
                println("scan hex error")
            }
        }
        
        let color: UIColor =  UIColor(red:CGFloat(red), green: CGFloat(green), blue:CGFloat(blue), alpha: 1.0)
        return color
    }


}